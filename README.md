# GitLab License Management

[![pipeline status](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/commits/master)
[![coverage report](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/commits/master)

GitLab tool for detecting licenses of the dependencies used by the provided source.
It is currently based on [License Finder][license_finder]
only, but this may change in the future.

## How to use

1. `cd` into the directory of the source code you want to scan
1. Run the Docker image:

    ```sh
    docker run \
      --volume "$PWD":/code \
      --rm \
      registry.gitlab.com/gitlab-org/security-products/analyzers/license-finder:latest analyze /code
    ```

1. The results will be stored in the `gl-license-scanning-report.json` file in the application directory.

## Development

### Running the application

License Management is a Docker image. You can build it like this from the project root:

```sh
$ ./bin/docker-build
```

You can then run License Management on some target directory:

```sh
$ docker run --rm --volume "/path/to/my/project":/code --env CI_PROJECT_DIR=/code
```

### Running the tests

You can run the tests from inside a docker container:

```sh
$ ./bin/docker-build
$ ./bin/docker-shell
$ ./bin/setup
$ ./bin/test [path to file]
```

If you need to debug any specific issues you can do this from within the docker container by
following these steps:

```sh
$ ./bin/docker-build
$ ./bin/docker-shell
$ enable_dev_mode
$ bundle open license_finder
```

The `docker-shell` script will mount the current project as a volume into `/builds/gitlab-org/security-products/analyzers/license-finder`.
This allows you to edit code from your host machine using your preferred editor and
see the affect of those changes from within the running docker container.

### Updating the SPDX index

We will need to periodically update the SPDX index. This can be achieved with
the following command.

```bash
$ ./bin/update-spdx
```

## Supported languages and package managers

The following table shows which languages and package managers are supported.

| Language   | Package managers                                                  |
|------------|-------------------------------------------------------------------|
| .NET       | [.NET Core CLI][dotnet_core], [Nuget][nuget]                      |
| C/C++      | [Conan][conan]                                                    |
| Go         | [Go modules][gomod], [Godep][godep], go get                       |
| Java       | [Gradle][gradle], [Maven v3.2.5+)][maven]                           |
| JavaScript | [npm][npm], [yarn][yarn], [Bower][bower]                          |
| PHP        | [composer][composer]                                              |
| Python     | [pip][pip], [pipenv][pipenv]                                      |
| Ruby       | [Bundler][bundler]                                                |

Inject `SETUP_CMD` to the docker command to override the given package managers
and run your custom command to setup your environment with a custom package manager.

```sh
docker run \
  --volume "$PWD":/code \
  --env "SETUP_CMD=./my-custom-install-script.sh" \
  --rm \
  registry.gitlab.com/gitlab-org/security-products/analyzers/license-finder:latest analyze /code
```

## Settings

The License Management tool can be customized with environments variables for some project types.

| Environment variable | Project type | Function |
|----------------------|--------------|----------|
| ADDITIONAL_CA_CERT_BUNDLE | * | Additional certificate chain to install in the trusted store. |
| MAVEN_CLI_OPTS       | Java (Maven) | Additional arguments for the mvn executable. If not supplied, defaults to `-DskipTests`. |
| LICENSE_FINDER_CLI_OPTS | * | Additional arguments for the `license_finder` executable. |
| LM_JAVA_VERSION      | Java (Maven) | Version of Java. If set to `11`, Maven and Gradle use Java 11 instead of Java 8. |
| LM_PYTHON_VERSION    | Python       | Version of Python. If set to `3`, dependencies are installed using Python 3 instead of Python 2.7. |
| LOG_LEVEL    | * | Control the verbosity of the logs. (`debug`, `info`, `warn` (default), `error`, `fatal`)  |
| LM_REPORT_FILE    | * | Name of the generated report. If not supplied, defaults to `gl-license-scanning-report.json`  |


Inject the required environment variables to the docker command using the [`--env` option flag](https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file)
or its shorthand form (`--env MY_SETTING_VAR`) if the configuration comes from an external environment.

## Versioning and release process

Please check the [Release Process documentation](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/release_process.md).

## Upgrading to the latest version of LicenseFinder

1. Check for the latest version of `LicenseFinder` at [https://rubygems.org/gems/license_finder][license_finder]
1. Check the version of the `license_finder` gem that is currently being used in the [`Gemfile.lock`][gemfile_lock]
1. If an update is available, create a new branch
1. Bump the license management version in [CHANGELOG.md][changelog] and in [version.rb][version_rb]
1. Update the `license_finder` version constraint in the [gemspec][gemspec]
1. Run `bundle update license_finder`
1. Test the changes by following the instructions for [running the tests](#running-the-tests)
1. Submit a merge request.

# Contributing

If you want to help, read the [contribution guidelines](CONTRIBUTING.md).

If an unknown license is detected, please consider updating the mapping defined
in [normalized-licenses.yml](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/blob/master/normalized-licenses.yml). A mapping can be for a detected name or url and must correspond to an SPDX identifier found in [spdx-licenses.json](https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/blob/master/spdx-licenses.json).

[bower]: https://bower.io/
[bundler]: https://bundler.io/
[changelog]: https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/blob/master/CHANGELOG.md
[composer]: https://getcomposer.org
[conan]: https://conan.io/
[dotnet_core]: https://docs.microsoft.com/en-us/dotnet/core/tools/
[gemfile_lock]: https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/blob/master/Gemfile.lock
[gemspec]: https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/blob/master/license-management.gemspec
[godep]: https://github.com/tools/godep
[gomod]: https://github.com/golang/go/wiki/Modules
[gradle]: https://gradle.org/
[license_finder]: https://rubygems.org/gems/license_finder
[maven]: https://maven.apache.org/
[npm]: https://www.npmjs.com/
[nuget]: https://www.nuget.org/
[pip]: https://pip.pypa.io/en/stable/
[pipenv]: https://github.com/pypa/pipenv
[version_rb]: https://gitlab.com/gitlab-org/security-products/analyzers/license-finder/-/blob/master/lib/license/management/version.rb
[yarn]: https://yarnpkg.com/
