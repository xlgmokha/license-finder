# frozen_string_literal: true

name "libsqlite3"
default_version "3.32.3"

dependency "config_guess"

source url: "https://www.sqlite.org/2020/sqlite-autoconf-3320300.tar.gz",
       sha1: "ea14ef2dc4cc7fcbc5ebbb018d3a03faa3a41cb4"

relative_path "sqlite-autoconf-3320300"

build do
  env = with_standard_compiler_flags(with_embedded_path)
  configure_command = [
    "--prefix=#{install_dir}/embedded",
    "--disable-nls"
  ]
  configure(*configure_command, env: env)
  make("-j #{workers}", env: env)
  make("-j #{workers} install", env: env)
end
