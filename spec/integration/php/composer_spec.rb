# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "composer" do
  subject { runner.scan(env: env) }

  let(:env) { {} }

  before do
    system("rm -rf /opt/asdf/installs/php/**/.composer")
  end

  include_examples "each report version", "php", "composer", "f65a9cf0"

  context "when the project's dependencies require php-gd e.g. in the case of Drupal" do
    before do
      runner.mount(dir: fixture_file('php/composer/drupal'))
    end

    it 'installs the required dependencies and produces a valid report' do
      expect(subject).to match_schema
      expect(subject[:version]).not_to be_empty
      expect(subject[:licenses]).not_to be_empty
      expect(subject.dependency_names).to match_array(%w[
        asm89/stack-cors
        brumann/polyfill-unserialize
        composer/semver
        doctrine/annotations
        doctrine/cache
        doctrine/collections
        doctrine/common
        doctrine/event-manager
        doctrine/inflector
        doctrine/lexer
        doctrine/persistence
        doctrine/reflection
        easyrdf/easyrdf
        egulias/email-validator
        guzzlehttp/guzzle
        guzzlehttp/promises
        guzzlehttp/psr7
        masterminds/html5
        paragonie/random_compat
        pear/archive_tar
        pear/console_getopt
        pear/pear-core-minimal
        pear/pear_exception
        psr/container
        psr/http-message
        psr/log
        ralouphie/getallheaders
        stack/builder
        symfony-cmf/routing
        symfony/class-loader
        symfony/console
        symfony/debug
        symfony/dependency-injection
        symfony/event-dispatcher
        symfony/http-foundation
        symfony/http-kernel
        symfony/polyfill-ctype
        symfony/polyfill-iconv
        symfony/polyfill-intl-idn
        symfony/polyfill-intl-normalizer
        symfony/polyfill-mbstring
        symfony/polyfill-php56
        symfony/polyfill-php70
        symfony/polyfill-php72
        symfony/polyfill-util
        symfony/process
        symfony/psr-http-message-bridge
        symfony/routing
        symfony/serializer
        symfony/translation
        symfony/validator
        symfony/yaml
        twig/twig
        typo3/phar-stream-wrapper
        zendframework/zend-diactoros
        zendframework/zend-escaper
        zendframework/zend-feed
        zendframework/zend-stdlib
      ])
    end
  end

  context "when scanning Drupal dependencies" do
    let(:env) { { 'SETUP_CMD' => 'bash setup.sh' } }

    before do
      runner.mount(dir: fixture_file('php/composer/drupal-core'))
    end

    it 'detects the licenses correctly' do
      expect(subject.licenses_for('drupal/core-composer-scaffold')).to match_array(['GPL-2.0-or-later'])
      expect(subject.licenses_for('drupal/core-project-message')).to match_array(['GPL-2.0-or-later'])
      expect(subject.licenses_for('drupal/core-recommended')).to match_array(['GPL-2.0-or-later'])
    end
  end

  context "when fetching dependencies from a custom registry" do
    before do
      runner.mount(dir: fixture_file('php/composer/custom-tls'))
    end

    context "when the CA certificate is provided" do
      let(:env) { { 'ADDITIONAL_CA_CERT_BUNDLE' => x509_certificate.read } }

      specify do
        expect(subject).to match_schema
        expect(subject.dependency_names).to match_array(['monolog/monolog'])
        expect(subject.licenses_for('monolog/monolog')).to match_array(['MIT'])
      end
    end

    context "when the CA certificate is NOT provided" do
      let(:env) { {} }

      specify { expect(subject).to match_schema }
    end
  end

  context "when scanning a project with dev dependencies" do
    before do
      runner.mount(dir: fixture_file('php/composer/dev-dependencies'))
    end

    it 'excludes the dev dependencies' do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(['monolog/monolog'])
      expect(subject.licenses_for('monolog/monolog')).to match_array(['MIT'])
    end
  end

  context "when scanning a project with a lock file and sourced from an unreachable network location" do
    before do
      runner.mount(dir: fixture_file('php/composer/unreachable-network'))
    end

    it 'parses the information from the lockfile' do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(['monolog/monolog'])
      expect(subject.licenses_for('monolog/monolog')).to match_array(['MIT'])
    end
  end
end
